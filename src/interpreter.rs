use crate::language::{Program, Function, Expression, Variant};
use std::{ops::Add, collections::HashMap};

pub trait BuiltinFunction {
    fn call(&self, input: &Vec<Variant>) -> Variant;
}

impl<F> BuiltinFunction for F
where
    F: Fn(&Vec<Variant>) -> Variant,
{
    fn call(&self, input: &Vec<Variant>) -> Variant {
        self(input)
    }
}

struct BoxedFunction<'a> {
    func: Box<dyn BuiltinFunction + 'a>
}

impl<'a> BoxedFunction<'a> {
    pub fn new<F>(function: F) -> Self
    where
        F: BuiltinFunction + 'a,
    {
        BoxedFunction {
            func: Box::new(function),
        }
    }
}

impl<'a> BuiltinFunction for BoxedFunction<'a> {
    fn call(&self, input: &Vec<Variant>) -> Variant {
        self.func.call(input)
    }
}

impl Add for Variant {
    type Output = Self;

    fn add(self, other: Self) -> Self {
        match self {
            Variant::NumberVariant(v1) => match other {
                Variant::NumberVariant(v2) => Variant::NumberVariant(v1 + v2),
                _ => panic!("Cannot add unlike types")
            },
            Variant::StringVariant(s1) => match other {
                Variant::StringVariant(s2) => Variant::StringVariant(s1 + &s2),
                _ => panic!("cannot add unlike types")
            },
            Variant::BoolVariant(_) => panic!("cannot add booleans")
        }
    }
}

fn builtin_add(input: &Vec<Variant>) -> Variant {
    let mut input_iter = input.iter();
    let first = match input_iter.next() {
        Some(var) => var.to_owned(),
        None => panic!("Missing parameters for `add(...)` function call")
    };
    input_iter.fold(first, |acc, item| acc + item.to_owned())
}


const MAIN_FN_NAME: &str = "main";

pub struct InterpreterContext<'a> {
    program: Program,
    builtin_functions: HashMap<String, BoxedFunction<'a>>
}

impl InterpreterContext<'_> {
    fn builtin_functions<'a>() -> HashMap<String, BoxedFunction<'a>> {
        let mut funcs = HashMap::new();

        // Math functions
        funcs.insert("add".to_string(), BoxedFunction::new(builtin_add));

        funcs
    }

    pub fn create(program: Program) -> Self {
        InterpreterContext{
            program,
            builtin_functions: Self::builtin_functions()
        }
    }

    fn interpret_expression(&self, expression: &Expression, fn_parameters: &HashMap<String, Variant>) -> Variant {
        match expression {
            Expression::Constant(value) => value.to_owned(),
            Expression::FunctionCall { identifier, parameters } => 
                self.interpret_function(
                    &identifier, 
                    &parameters.iter().map(
                        |expr| self.interpret_expression(expr, fn_parameters)
                    ).collect::<Vec<_>>()
                ),
            Expression::FunctionParam(identifier) => match fn_parameters.get(identifier) {
                Some(parameter) => parameter.to_owned(),
                None => panic!("Invalid function parameter in expression: {}", identifier)
            }
        }
    }
    
    fn make_parameter_hashmap(function: &Function, parameters: &Vec<Variant>) -> HashMap<String, Variant> {
        assert_eq!(function.parameters.len(), parameters.len());
        let mut parameters_iter = parameters.iter();
        let mut parameters_map = HashMap::new();
        function.parameters.iter().for_each(|param_name| {
            parameters_map.insert(param_name.clone(), parameters_iter.next().unwrap().to_owned());
        });
        parameters_map
    }

    fn interpret_function(&self, fn_name: &String, parameters: &Vec<Variant>) -> Variant {
        match self.program.functions.get(fn_name) {
            Some(func) => self.interpret_expression(&func.expression, &Self::make_parameter_hashmap(func, parameters)),
            None => match self.builtin_functions.get(fn_name) {
                Some(func) => func.call(parameters),
                None => panic!("Function does not exist: {}", fn_name)
            }
        }
    }

    pub fn run(&self) -> Variant {
        match self.program.functions.get("main") {
            Some(function) => self.interpret_expression(&function.expression, &HashMap::new()),
            None => panic!("No main function in program")
        }
    }

    fn is_expression_valid(&self, expression: &Expression, context: &Function) -> bool {
        match expression {
            Expression::Constant(_) => true,
            Expression::FunctionCall { identifier, parameters } =>
                (self.program.functions.contains_key(identifier) || self.builtin_functions.contains_key(identifier))
                    && parameters.iter().all(|expression: &Expression| self.is_expression_valid(expression, context)),
            Expression::FunctionParam(identifier) => context.parameters.contains(identifier)
        }
    }

    pub fn is_valid<'a>(&self) -> Result<(), String> {
        let validate_function = |function: &Function| -> Result<(), String> {
            match self.is_expression_valid(&function.expression, function) {
                true => Ok(()),
                false => Err(format!("Function {} references invalid functions/parameters", function.name))
            }
        };

        match self.program.functions.values().find(|function| validate_function(function).is_err()) {
            Some(function) => validate_function(function),
            None => match self.program.functions.contains_key(&MAIN_FN_NAME.to_string()) {
                true => Ok(()),
                false => Err("Main function not found".to_string())
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::language::Variant;

    #[test]
    fn builtin_add() {
        assert_eq!(
            super::builtin_add(&vec![
                Variant::StringVariant("hello".to_string()), 
                Variant::StringVariant(" ".to_string()), 
                Variant::StringVariant("world".to_string())]
            ), 
            Variant::StringVariant("hello world".to_string())
        )
    }
}
