use crate::parser::*;
use std::collections::HashMap;
use serde::Serialize;
use serde::Deserialize;

/*
Program structure:

fn function_name (param, param2) = [expression];
fn second_function (param, param2) = [expression];
fn main() = [expression];
...

Expression structure:
[Literal, function call, or function param]
Ex:
true
add(7, 5)
multiply(increment(bar), 7)

Literals:
1.23
-10
true
"foo"

Function calls:
foo([expression], [expression], ...)

*/

#[derive(Debug, PartialEq, PartialOrd, Clone, Serialize, Deserialize)]
pub enum Variant {
    NumberVariant(f64),
    StringVariant(String),
    BoolVariant(bool)
}

fn parse_boolean<'a>() -> BoxedParser<'a, Variant> {
    BoxedParser::new(
        either(
            match_literal("true").map(|()| Variant::BoolVariant(true)), 
            match_literal("false").map(|()| Variant::BoolVariant(false))
        )
    )
}

fn parse_string<'a>() -> BoxedParser<'a, Variant> {
    BoxedParser::new(
        match_literal("\"").and_then(|_| match_until("\"")).map(|string| Variant::StringVariant(string))
    )
}

fn string_to_float(number_string: String) -> Variant {
    match number_string.parse::<f64>() {
        Ok(value) => Variant::NumberVariant(value),
        Err(_) => panic!("Failed to parse number. Not sure how this happened, it shouldn't be possible :/")
    }
}

fn parse_number<'a>() -> BoxedParser<'a, Variant> {
    BoxedParser::new(
        optional(match_literal("-"))
        .map(|output| match output {Some(_) => "-", _ => ""})
        .and_then(|sign|
            one_or_more(
                match_character(|c| c.is_numeric())
            ).and_then(
                move |whole_digits| optional(
                    both(
                        match_literal("."),
                        one_or_more(match_character(|c| c.is_numeric()))
                    )
                ).map(
                    move |decimal_digits| match decimal_digits {
                        Some((_, digits)) => 
                            string_to_float(sign.to_owned() + &whole_digits.iter().collect::<String>() + "." + &digits.iter().collect::<String>()),
                        None => string_to_float(sign.to_owned() + &whole_digits.iter().collect::<String>())
                    }
                )
            )
        )
        
    )
}

fn parse_variant<'a>() -> BoxedParser<'a, Variant> {
    BoxedParser::new(
        either(
            either(
                parse_string(),
                parse_number()
            ),
            parse_boolean()
        )
    )
}

#[derive(Debug, PartialEq, PartialOrd, Clone, Serialize, Deserialize)]
pub enum Expression {
    Constant(Variant),
    FunctionCall{identifier: String, parameters: Vec<Expression>},
    FunctionParam(String)
}

fn parse_constant<'a>() -> BoxedParser<'a, Expression> {
    BoxedParser::new(
        parse_variant().map(|variant| Expression::Constant(variant))
    )
}

// An identifier is _ or a letter followed by any number of alphanumeric characters
// Ex: _ foo an_ident _3 _bar Foo3
fn parse_identifier<'a>() -> impl Parser<'a, String> {
    match_character(|c| c.is_alphabetic() || *c == '_')
    .and_then(|first_char|
        zero_or_more(
            match_character(|c| c.is_alphanumeric() || *c == '_')
        ).map(
            move |chars| first_char.to_string() + &chars.into_iter().collect::<String>()
        )
    )
}

fn parse_function_call<'a>() -> BoxedParser<'a, Expression> {
    BoxedParser::new(
        whitespace_wrap(parse_identifier()).and_then(move |ident: String|
            right(both(
                match_literal("("),
                left(both(
                    optional(zero_or_more(
                        left(both(
                            parse_expression(), match_literal(",")
                        ))
                    ).and_then(move |parameters: Vec<Expression>|
                        parse_expression().map(move|last_param| parameters.iter().cloned().chain(vec![last_param].into_iter()).collect())
                    )).map(move |output: Option<Vec<Expression>>| match output{Some(params) => params, _ => vec![]}),
                    both(zero_or_more(match_whitespace()), match_literal(")"))
                ))
            )).map(move |parameters|
                Expression::FunctionCall{identifier: ident.clone(), parameters}
            )
        )
    )
}

fn parse_expression<'a>() -> BoxedParser<'a, Expression> {
    BoxedParser::new(
        whitespace_wrap(either(
            parse_constant(),
            either(
                parse_function_call(),
                parse_identifier().map(|ident| Expression::FunctionParam(ident))
            )
        ))
    )
}

#[derive(Debug, PartialOrd, PartialEq, Clone, Serialize, Deserialize)]
pub struct Function {
    pub name: String,
    pub parameters: Vec<String>,
    pub expression: Expression
}


fn parse_function_definition<'a>() -> BoxedParser<'a, (String, Vec<String>)> {
    BoxedParser::new(
        right(both(
            // Match fn literal
            match_literal("fn"),
            // Match function name
            whitespace_wrap(parse_identifier()).and_then(|name|
                right(both(
                    // Match opening parentheses
                    match_literal("("),
                    // Match contents & closing parentheses
                    left(both(
                        // Match parameters with commas
                        optional(zero_or_more(
                            left(both(
                                whitespace_wrap(parse_identifier()), match_literal(",")
                            ))
                        ).and_then(move |parameters|
                            // Match last parameter and append it
                            whitespace_wrap(parse_identifier())
                                .map(move|last_param| parameters.iter().cloned().chain(vec![last_param].into_iter()).collect())
                        )).map(move |output| match output{
                            Some(params) => (name.clone(), params), None => (name.clone(), vec![])
                        }),
                        both(zero_or_more(match_whitespace()), match_literal(")"))
                    ))
                ))
            )
        ))
    )
}

fn parse_function<'a>() -> BoxedParser<'a, Function> {
    BoxedParser::new(
            whitespace_wrap(parse_function_definition())
            .and_then(|(name, params)|
            right(both(
                match_literal("="),
                parse_expression()
            )).map(move |expression|
                Function{name: name.clone(), parameters: params.clone(), expression}
            )
        )
    )
}

#[derive(Debug, Serialize, Deserialize)]
pub struct Program {
    pub functions: HashMap<String, Function>
}


fn parse_program<'a>() -> BoxedParser<'a, Program> {
    BoxedParser::new(
        zero_or_more(
            left(both(
                whitespace_wrap(parse_function()),
                match_literal(";")
            ))
        ).map(|functions| 
            Program{
                functions: functions.into_iter().map(
                    |func| (func.name.clone(), func)
                ).collect()
            }
        )
    )
}

pub fn parse_from_string(input: &String) -> Result<Program, &str> {
    let program_parser = parse_program();
    program_parser.parse(input)
        .map(|(_rem, program)| program)
}

#[cfg(test)]
mod tests {
    use crate::parser::Parser;

    #[test]
    fn parse_variant_bool() {
        let parse_variant = super::parse_variant();

        assert_eq!(parse_variant.parse("true"), Ok(("", super::Variant::BoolVariant(true))));
        assert_eq!(parse_variant.parse("false"), Ok(("", super::Variant::BoolVariant(false))));
        
    }

    #[test]
    fn parse_variant_string() {
        let parse_variant = super::parse_variant();
        
        assert_eq!(parse_variant.parse("\"foo\""), Ok(("", super::Variant::StringVariant("foo".to_string()))));
        assert_eq!(parse_variant.parse("\"\"asdf"), Ok(("asdf", super::Variant::StringVariant("".to_string()))));
    }

    #[test]
    fn parse_variant_number() {
        let parse_variant = super::parse_variant();
        
        assert_eq!(
            parse_variant.parse("123"),
            Ok(("", super::Variant::NumberVariant(123.0)))
        );
        assert_eq!(
            parse_variant.parse("0.0"),
            Ok(("", super::Variant::NumberVariant(0.0)))
        );
        assert_eq!(
            parse_variant.parse("-0.0"),
            Ok(("", super::Variant::NumberVariant(0.0)))
        );
        assert_eq!(
            parse_variant.parse("-0.01"),
            Ok(("", super::Variant::NumberVariant(-0.01)))
        );

        assert!(parse_variant.parse("foo").is_err());
        assert!(parse_variant.parse(".1").is_err());
    }

    #[test]
    fn parse_identifier() {
        let parse_ident = super::parse_identifier();

        assert!(parse_ident.parse("123").is_err());
        assert!(parse_ident.parse(" asdf").is_err());
        assert!(parse_ident.parse("-").is_err());

        assert_eq!(parse_ident.parse("Foo"), Ok(("", "Foo".to_string())));
        assert_eq!(parse_ident.parse("foo_bar"), Ok(("", "foo_bar".to_string())));
        assert_eq!(parse_ident.parse("_variable"), Ok(("", "_variable".to_string())));
        assert_eq!(parse_ident.parse("thing1,thing2"), Ok((",thing2", "thing1".to_string())));
    }

    #[test]
    fn parse_expression() {
        let parse_expression = super::parse_expression();

        assert_eq!(parse_expression.parse(" true  )"), Ok((")", super::Expression::Constant(super::Variant::BoolVariant(true)))));
        assert_eq!(
            parse_expression.parse(" foo ( 10, false )asdf"),
            Ok(("asdf", super::Expression::FunctionCall{
                identifier: "foo".to_string(),
                parameters: vec![
                    super::Expression::Constant(super::Variant::NumberVariant(10.0)),
                    super::Expression::Constant(super::Variant::BoolVariant(false))
                ]
            }))
        );
        
        assert_eq!(
            parse_expression.parse(" _bar ( ) "),
            Ok(("", super::Expression::FunctionCall{
                identifier: "_bar".to_string(),
                parameters: vec![]
            }))
        );

        assert_eq!(
            parse_expression.parse(" asdf "),
            Ok(("", super::Expression::FunctionParam("asdf".to_string())))
        );

        assert!(parse_expression.parse("-asdf").is_err())
    }

    #[test]
    fn parse_function_definition() {
        let parse_function_definition = super::parse_function_definition();

        assert_eq!(
            parse_function_definition.parse("fn foo(foo,bar)=asdf"),
            Ok(("=asdf", ("foo".to_string(), vec!["foo".to_string(), "bar".to_string()])))
        );
    }

    #[test]
    fn parse_function() {
        let parse_func = super::parse_function();

        assert_eq!(
            parse_func.parse("fn foo(foo, bar) = foo"),
            Ok(("", super::Function{name: "foo".to_string(), parameters: vec!["foo".to_string(), "bar".to_string()], expression: super::Expression::FunctionParam("foo".to_string())})) //("foo".to_string(), vec!["foo".to_string(), "bar".to_string()])
        );

        assert!(parse_func.parse("fn bar(foo ,fizz,foo2, _a) = if(_a, foo, bar(foo, fizz, foo2, sub(_a, 1)))").is_ok())
    }

    #[test]
    fn parse_program() {
        let parse_program = super::parse_program();

        assert!(parse_program.parse("fn main() = true;").is_ok());
    }
}
