mod parser;
mod language;
mod interpreter;
use crate::language::{Program, parse_from_string};
extern crate clap;
use clap::{Arg, App, crate_version};

use std::fs::File;
use std::io::prelude::*;
use std::path::Path;
use interpreter::InterpreterContext;


fn parse_program_from_file(path: &Path) -> Program {
    let display = path.display();
    // Open the path in read-only mode, returns `io::Result<File>`
    let mut file = match File::open(&path) {
        Err(why) => panic!("couldn't open {}: {}", display, why),
        Ok(file) => file,
    };

    // Read the file contents into a string, returns `io::Result<usize>`
    let mut s = String::new();
    match file.read_to_string(&mut s) {
        Err(why) => panic!("couldn't read {}: {}", display, why),
        Ok(_) => {},
    };

    match parse_from_string(&s) {
        Ok(program) => program,
        Err(why) => panic!("Failed to parse program: {}", why)
    }
}

fn main() {
    let app = App::new("Disfunctional")
        .version(crate_version![])
        .author("Jack LeFevre (JackLeFevre.com)")
        .about("Interpreter for the Disfunctional programming language")
        .arg(Arg::with_name("input")
            .short("i")
            .long("input")
            .value_name("INPUT FILE")
            .help("Sets the input file")
            .takes_value(true))
        .arg(Arg::with_name("output")
            .short("o")
            .long("output")
            .value_name("OUTPUT FILE")
            .help("Specifies an output file for the parsed program. This will parse the program to an intermediate representation and store it to a file.")
            .takes_value(true));

    let matches = app.get_matches();
    
    let input_matches = matches.value_of("input");

    if input_matches.is_some() {
        let input_filename = input_matches.unwrap();
        let input_file_path = Path::new(input_filename);

        let program = parse_program_from_file(input_file_path);


        let interpreter_context = InterpreterContext::create(program);
        match interpreter_context.is_valid() {
            Ok(_) => {}
            Err(why) => panic!("Program is not valid: {}", why)
        }

        match matches.value_of("output") {
            Some(output_filename) => println!("Input file name: {}\nOuput file name: {}", input_filename, output_filename),
            None => {
                let result = interpreter_context.run();

                println!("{:?}", result);
            }
        }
    } else {
        println!("No input file specified. Please see --help for more info.")
    }
}