// The result of a parser
pub type ParserResult<'a, Output> = Result<(&'a str, Output), &'a str>;

// The generic parser type
pub trait Parser<'a, Output> {
    fn parse(&self, input: &'a str) -> ParserResult<'a, Output>;

    fn map<PredicateType, NewOutput>(self, predicate: PredicateType) -> BoxedParser<'a, NewOutput>
    where
        Self: Sized + 'a,
        Output: 'a,
        NewOutput: 'a,
        PredicateType: Fn(Output) -> NewOutput + 'a,
    {
        BoxedParser::new(map(self, predicate))
    }

    fn pred<PredicateType>(self, predicate: PredicateType) -> BoxedParser<'a, Output>
    where
        Self: Sized + 'a,
        Output: 'a,
        PredicateType: Fn(&Output) -> bool + 'a,
    {
        BoxedParser::new(pred(self, predicate))
    }

    fn and_then<F, NewParser, NewOutput>(self, f: F) -> BoxedParser<'a, NewOutput>
    where 
        Self: Sized + 'a,
        F: Fn(Output) -> NewParser + 'a,
        NewParser: Parser<'a, NewOutput> + 'a,
        Output: 'a,
        NewOutput: 'a,
    {
        BoxedParser::new(and_then(self, f))
    }

}

// Boxed parser avoids deeply nested/exponentially recursive types
// because the compiler doesn't really like that.
pub struct BoxedParser<'a, Output> {
    parser: Box<dyn Parser<'a, Output> + 'a>,
}

impl<'a, Output> BoxedParser<'a, Output> {
    pub fn new<P>(parser: P) -> Self
    where
        P: Parser<'a, Output> + 'a,
    {
        BoxedParser {
            parser: Box::new(parser),
        }
    }
}

// Allows using any function with the correct signature as a Parser
impl<'a, F, Output> Parser<'a, Output> for F
where
    F: Fn(&'a str) -> ParserResult<Output>,
{
    fn parse(&self, input: &'a str) -> ParserResult<'a, Output> {
        self(input)
    }
}

impl<'a, Output> Parser<'a, Output> for BoxedParser<'a, Output> {
    fn parse(&self, input: &'a str) -> ParserResult<'a, Output> {
        self.parser.parse(input)
    }
}

// Returns a parser function that matches the given literal
pub fn match_literal<'a>(literal: &'a str) -> impl Parser<'a, ()> {
    move |input: &'a str| match input.get(0..literal.len()) {
        Some(string) if string == literal => Ok((&input[literal.len()..], ())),
        _ => Err("Could not find literal")
    }
}

pub fn any_character<'a>(input: &'a str) -> ParserResult<char> {
    match input.chars().next() {
        Some(character) => Ok((&input[1..], character)),
        _ => Err("No character found")
    }
}

pub fn pred<'a, PredicateType, ParserType, Output>(parser: ParserType, predicate: PredicateType) -> impl Parser<'a, Output>
where 
    PredicateType: Fn(&Output) -> bool,
    ParserType: Parser<'a, Output>
{
    move |input: &'a str| match parser.parse(input) {
        Ok((remainder, output)) if predicate(&output) => Ok((remainder, output)),
        _ => Err("Did not match predicate")
    }
}


// Returns a parser that matches any character for which predicate returns true
pub fn match_character<'a, F>(predicate: F) -> impl Parser<'a, char>
where F: Fn(&char) -> bool + 'a {
    any_character.pred(predicate)
}

pub fn match_whitespace<'a>() -> impl Parser<'a, char> {
    match_character(|c| c.is_whitespace())
}

// Returns a parser function that matches anything until the given string
pub fn match_until<'a>(terminator: &'a str) -> impl Parser<'a, String> {
    move |input: &'a str| match input.find(terminator) {
        Some(position) => Ok((&input[position + terminator.len() ..], input[..position].to_string())),
        None => Err("Could not find valid character")
    }
}

// Returns a parser that always succeeds and returns an optional output
pub fn optional<'a, F, Output>(parser: F) -> impl Parser<'a, Option<Output>> 
    where F: Parser<'a, Output> {
    move |input| match parser.parse(input) {
        Ok(result) => Ok((result.0, Some(result.1))),
        Err(_) => Ok((input, None))
    }
}


// Combinator that accepts two parsers in order
pub fn both<'a, F1, O1, F2, O2>(first: F1, second: F2) -> impl Parser<'a, (O1, O2)>
where 
    F1: Parser<'a, O1>, 
    F2: Parser<'a, O2>
{
    move |input: &'a str| match first.parse(input) {
        Ok((first_remainder, first_result)) => {
            match second.parse(first_remainder) {
                Ok((remainder, second_result)) => Ok((remainder, (first_result, second_result))),
                Err(err) => Err(err)
            }
        },
        Err(err) => Err(err)
    }
}

pub fn either<'a, F1, F2, Output>(first: F1, second: F2) -> impl Parser<'a, Output>
where
    F1: Parser<'a, Output>,
    F2: Parser<'a, Output>,
{
    move |input: &'a str| match first.parse(input) {
        ok @ Ok(_) => ok,
        Err(_) => second.parse(input)
    }
}
    
pub fn map<'a, ParserType, PredicateType, A, B>(parser: ParserType, predicate: PredicateType) -> impl Parser<'a, B>
where
    ParserType: Parser<'a, A>,
    PredicateType: Fn(A) -> B
{
    move |input: &'a str| parser.parse(input).map(|(remainder, output)| (remainder, predicate(output)))
}

pub fn left<'a, ParserType, A, B> (parser: ParserType) -> impl Parser<'a, A>
where ParserType: Parser<'a, (A, B)>
{
    map(parser, |(a, _)| a)
}

pub fn right<'a, ParserType, A, B> (parser: ParserType) -> impl Parser<'a, B>
where ParserType: Parser<'a, (A, B)>
{
    map(parser, |(_, b)| b)
}

fn match_zero_or_more<'a, ParserType, Output>(parser: &ParserType, input: &'a str) -> (&'a str, Vec<Output>)
where ParserType: Parser<'a, Output>{
    match parser.parse(input) {
        Ok((remainder, output)) => {
            let (parsed_remainder, parsed_output) = match_zero_or_more(parser, remainder);

            // Gross mutable extendy stuff
            let mut extended_output = vec![output];
            extended_output.extend(parsed_output);
            (parsed_remainder, extended_output)
        },
        Err(_) => (input, vec![])
    }
}

pub fn zero_or_more<'a, ParserType, Output> (parser: ParserType) -> impl Parser<'a, Vec<Output>>
where ParserType: Parser<'a, Output> {
    move |input: &'a str| Ok(match_zero_or_more(&parser, input))
}

pub fn one_or_more<'a, ParserType, Output> (parser: ParserType) -> impl Parser<'a, Vec<Output>>
where ParserType: Parser<'a, Output> {
    move |input: &'a str| {
        let (remainder, zero_or_more) = match_zero_or_more(&parser, input);
        match zero_or_more.len() {
            0 => Err("Did not match pattern"),
            _ => Ok((remainder, zero_or_more))
        }
    }
}

pub fn and_then<'a, P, F, A, B, NextP>(parser: P, f: F) -> impl Parser<'a, B>
where
    P: Parser<'a, A>,
    NextP: Parser<'a, B>,
    F: Fn(A) -> NextP,
{
    move |input| match parser.parse(input) {
        Ok((next_input, result)) => f(result).parse(next_input),
        Err(err) => Err(err),
    }
}

pub fn whitespace_wrap<'a, P, Output>(parser: P) -> impl Parser<'a, Output>
where P: Parser<'a, Output>
{
    left(
        both(
            right(
                both(
                    zero_or_more(match_whitespace()), 
                    parser
                )
            ), 
            zero_or_more(match_whitespace())
        )
    )
}

#[cfg(test)]
mod tests {
    use crate::parser::Parser;

    #[test]
    fn test_match_literal() {
        let literal_matcher = super::match_literal("foo");
        let test_str = "fooasdf";
        let result = literal_matcher.parse(test_str);

        assert!(result.is_ok());
        
        assert_eq!(result.unwrap(), ("asdf", ()));
    }

    #[test]
    fn test_match_literal_only() {
        let literal_matcher = super::match_literal("foo");
        let test_str = "foo";
        let result = literal_matcher.parse(test_str);

        assert!(result.is_ok());
        
        assert_eq!(result.unwrap(), ("", ()));
    }

    #[test]
    fn test_no_match_literal() {
        let literal_matcher = super::match_literal("foo");
        let test_str = "asdf";
        let result = literal_matcher.parse(test_str);

        assert!(result.is_err());
    }

    #[test]
    fn test_match_character() {
        let character_matcher = super::match_character(|character| character.is_alphabetic());
        let test_str = "asdf";
        let result = character_matcher.parse(test_str);

        assert!(result.is_ok());
        assert_eq!(result.unwrap(), ("sdf", 'a'));
    }

    #[test]
    fn test_no_match_character() {
        let character_matcher = super::match_character(|character| character.is_alphabetic());
        let test_str = "1asdf";
        let result = character_matcher.parse(test_str);

        assert!(result.is_err());
    }

    #[test]
    fn test_parse_until() {
        let parse_until_quote = super::match_until("\"");
        let test_str = "foo\"asdf";
        let result = parse_until_quote.parse(test_str);

        assert!(result.is_ok());
        
        assert_eq!(result.unwrap(), ("asdf", "foo".to_string()));
    }

    #[test]
    fn test_parse_until_fail() {
        let parse_until_quote = super::match_until("\"");
        let test_str = "fooasdf";
        let result = parse_until_quote.parse(test_str);

        assert!(result.is_err());
    }

    #[test]
    fn test_optional_match() {
        let match_literal = super::match_literal("foo");
        let parse_optional = super::optional(match_literal);
        let test_str = "fooasdf";
        let result = parse_optional.parse(test_str);

        assert!(result.is_ok());
        let result = result.unwrap();
        
        assert_eq!(result.0, "asdf");
        assert_eq!(result.1, Some(()));
    }

    #[test]
    fn test_optional_no_match() {
        let match_literal = super::match_literal("foo");
        let parse_optional = super::optional(match_literal);
        let test_str = "asdf";
        let result = parse_optional.parse(test_str);

        assert!(result.is_ok());
        let result = result.unwrap();
        
        assert_eq!(result.0, "asdf");
        assert_eq!(result.1, None);
    }

    #[test]
    fn test_both() {
        let match_string = super::both(super::match_literal("\""), super::match_until("\""));
        let test_str = "\"asdf\"foo";
        let result = match_string.parse(test_str);

        assert!(result.is_ok());
        assert_eq!(result.unwrap(), ("foo", ((), "asdf".to_string())));
    }

    #[test]
    fn test_either() {
        let match_either = super::either(super::match_literal("Foo"), super::match_literal("Bar"));
        assert_eq!(match_either.parse("Foo"), Ok(("", ())));
        assert_eq!(match_either.parse("Bar"), Ok(("", ())));
        assert!(match_either.parse("baz").is_err());
    }

    #[test]
    fn test_map() {
        let match_string = 
            super::map(
                super::both(
                    super::match_literal("\""), 
                    super::match_until("\"")),
                |(_, string)| string
            );

        let test_str = "\"asdf\"foo";
        let result = match_string.parse(test_str);

        assert!(result.is_ok());
        assert_eq!(result.unwrap(), ("foo", "asdf".to_string()));
    }

    #[test]
    fn test_left() {
        let match_string = 
            super::left(
                super::both(
                    super::match_until("\""), 
                    super::match_until("\""))
            );

        let test_str = "foo\"bar\"asdf";
        let result = match_string.parse(test_str);

        assert!(result.is_ok());
        assert_eq!(result.unwrap(), ("asdf", "foo".to_string()));
    }

    #[test]
    fn test_right() {
        let match_string = 
            super::right(
                super::both(
                    super::match_until("\""), 
                    super::match_until("\""))
            );

        let test_str = "foo\"bar\"asdf";
        let result = match_string.parse(test_str);

        assert!(result.is_ok());
        assert_eq!(result.unwrap(), ("asdf", "bar".to_string()));
    }

    #[test]
    fn test_zero_or_more() {
        let match_letters = super::map(
            super::zero_or_more(super::match_character(|c| c.is_alphabetic())),
            |char_vec| -> String {char_vec.iter().collect()}
        );
        let test_str = "foo123";
        let result = match_letters.parse(test_str);
        assert!(result.is_ok());
        assert_eq!(result.unwrap(), ("123", "foo".to_string()))
    }

    #[test]
    fn test_zero_or_more_zero() {
        let match_letters = super::map(
            super::zero_or_more(super::match_character(|c| c.is_alphabetic())),
            |char_vec| -> String {char_vec.iter().collect()}
        );
        let test_str = "123";
        let result = match_letters.parse(test_str);
        assert!(result.is_ok());
        assert_eq!(result.unwrap(), ("123", "".to_string()))
    }

    #[test]
    fn test_one_or_more() {
        let match_letters = super::map(
            super::one_or_more(super::match_character(|c| c.is_alphabetic())),
            |char_vec| -> String {char_vec.iter().collect()}
        );
        let test_str = "foo123";
        let result = match_letters.parse(test_str);
        assert!(result.is_ok());
        assert_eq!(result.unwrap(), ("123", "foo".to_string()))
    }
    
    #[test]
    fn test_one_or_more_zero() {
        let match_letters = super::map(
            super::one_or_more(super::match_character(|c| c.is_alphabetic())),
            |char_vec| -> String {char_vec.iter().collect()}
        );
        let test_str = "213";
        let result = match_letters.parse(test_str);
        assert!(result.is_err());
    }
}

